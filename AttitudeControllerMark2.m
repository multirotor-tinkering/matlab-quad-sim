function des_pwm = AttitudeControllerMark2(state, desired)
    % des_nputs:
        % state - 3x5, usual state matrix variable
        % desired: 4x1, [thrust; yaw_rate; pitch; roll];
        % Thrust - PWM value
        % yaw rate - rad/s
        % pitch - rad
        % roll - rad
    % outputs:
        % inputs - 4x1, PWM inputs to individual motors, 
     
    % function:
        % apply given thrust, P on yaw rate, PD on roll and pitch

    % Get states
    % state_measured_last = state_measured;
    % state_measured(1) = [x;y;z]                   world frame
    % state_measured(2) = [dx; dy; dz]              world frame
    % state_measured(3) = [phi; theta; psi]         ~body/world? frame       (roll; pitch; yaw)
    % state_measured(4) = [dphi; dtheta; dpsi]      ~body/world? frame
    % state_measured(5) = [omega_x; omega_y; omega_z]  
    d_psi = state(3,4); 
    phi = state(1,3); d_phi = state(1,4);
    theta = state(2,3);  d_theta = state(2,4);
    
    thrust = desired(1); yaw_rate = desired(2); pitch = desired(3); roll = desired(4);
    % Do PID variables
    Kp_yaw = 100;
    Kp_pitch = 250; Kd_pitch = 25;
    Kp_roll = 250; Kd_roll = 25;

    % direct control on thrust
    T = thrust; % T is the force

    % P control on yaw
    Y = Kp_yaw * (yaw_rate-d_psi);

    % PD control on pitch
    P = Kp_pitch * (pitch - theta) + Kd_pitch * (0 - d_theta);

    % PD control on roll
    R = Kp_roll * (roll - phi) + Kd_roll * (0 - d_phi);

    TPRY = [T; P; R; Y];

    tpry2m = [ 1  1 -1  1;
               1  1  1 -1;
               1 -1  1  1;
               1 -1 -1 -1];

    des_pwm = tpry2m * (TPRY);

    % enfroce maximums
    des_pwm = max(des_pwm, 0);
    des_pwm = min(des_pwm, 1000);
    % add a 1000 for pwm
    des_pwm = des_pwm + 1000;
end